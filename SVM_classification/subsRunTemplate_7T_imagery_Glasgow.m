%% wrapper to compute cross-validated SVM classification
% code from the Muckli lab (with adjustments for this project)

clear all;
close all;


voi_appen_list={ '_peripheral';}; % ''; central or peripheral volume-of-interest
trueNSubs=1; %  cycle through several subjects, but keep all data separate files


for v=1:length(voi_appen_list)
    
    clearvars -except v voi_appen_list  trueNSubs

    for itrueSub=1:trueNSubs 

        nSubs=1; %keep 1 to keep all data separate
        voxSize=zeros(nSubs,2);
        voi_appen=voi_appen_list{v};


        for sub=1:nSubs

            clearvars -except v voi_appen_list voi_appen trueNSubs itrueSub nSubs voxSize sub %just to be on the safe side


            if(itrueSub==1) %add more if analyzing several subjects
                  subject='SUBJECT_ID';
                  sdate='SCAN_DATE';
            end



            nVOI = 6; % 6 cortical depths volumes-of-interest
            flag=1; % choose SVM normalization method (1 is recommended, puts between -1 and 1 per feature)
            perm = 0;%on = 1 or off = 0  (within-subject permutation testing)


            pairs=[1 2; 3 4; 5 6; 7 8; 9 10; 1 11; 3 9; 4 10; 7 9; 8 10; 5 9; 6 10; 5 7; 6 8; 1 3; 2 4; 1 5; 2 6; 3 5; 4 6]; 
            % 1 vs 2 = Perception (1)
            % 3 vs 4 = NCS, ie. illusory perception (2)
            % 5 vs 6 = Mental Imagery (3)
            % 7 vs 8 = amodal NCS (4)
            % 9 vs 10 = mock NCS (5)
            % 1 vs 11 = perception R against checkerboard target (6)
            % 3 vs 9 = NCS_R vs mock_R (7)
            % 4 vs 10 = NCS_G vs mock_G (8)
            % 7 vs 9 = amodal_R vs mock_R (9)
            % 8 vs 10 = amodal_G vs mock_G (10)
            % 5 vs 9 = Mental Imagery_R vs mock_R (11)
            % 6 vs 10 = Mental Imagery_G vs mock_G (12)
            % 5 vs 7 = Mental Imagery_R vs amodal_R (13)
            % 6 vs 8 = Mental Imagery_G vs amodal_G (14)
            % 1 vs 3 = Perception_R vs NCS_R (15)
            % 2 vs 4 = Perception_G vs NCS_G (16)
            % 1 vs 5 = Perception_R vs Mental Imagery_R (17)
            % 2 vs 6 = Perception_G vs Mental Imagery_G (18)
            % 3 vs 5 = NCS_R vs Mental Imagery_R (19)
            % 4 vs 6 = NCS_G vs Mental Imagery_G (20)

            nPairs=size(pairs,1);

            pc_sb=[];
            pc_av=[];



                for voi_indx=1:nVOI

                    % just call this function once per VOI now
                   [outD]=read_data_v5_7T_imagery_Glasgow(subject,sdate,voi_indx, voi_appen);

                   voxSize(sub,1)=size(outD.data,2);
                   voxSize(sub,2)=size(outD.stackB,2);

                   for i=1:nPairs    

                        locs=outD.stackCM(:,1)==pairs(i,1) | outD.stackCM(:,1)==pairs(i,2);
                        data=outD.stackB(locs,:);% data (beta weights)
                        labels=outD.stackCM(locs,1); %condition labels
                        XVlabels=outD.stackCM(locs,2); %run labels

                        [svmOut]=run_classifier_v2(data, labels, XVlabels,flag,perm);%

                        pc_sb(i,:)=svmOut.pc(:,2)';  %% single blocks
                        pc_av(i,:)=svmOut.pc(:,3)';  %% average (classification accuracy values of each fold)


                    end

                    subRes(:,sub,1)=mean(pc_sb,2); % single trial
                    figure;
                    imagesc(pc_sb(1:4,1:4)); 
                    figure(gcf)
                    subRes(:,sub,2)=mean(pc_av,2); % average %the classification accuracy we're interested in 
                    voiname=outD.voiname;
                    list_removed_vox=outD.list_removed_vox; %%voxels that have been removed from ROI because  mean BOLD<100 (in case of interest)


                    savename=[subject '_Classify_Data' voi_appen '_' outD.voiname(end-3:end) '.mat'];
                    save (savename, 'coll', 'subject', 'subRes', 'voiname');


                end   %% end subject loop

        end

    end
end

                                  