function [bbx,mask1, outV]=voi2msk_ACPC(sub, path_vtc,path_voi,voiname,vtc1, indx, pRF_Mask)
% vtc1 = vtc file name
%% function [bbx,mask1]=voi2msk(path_func,path_anat,voiname,vtc1)
% converts voi to msk in ACPC space
%
% either if more than one voi with keyword was found or if no voi was found
% clear all
% clc

% addpath('PATH_TO\BVQXtools_v08b'); % or Neuroelf v.09c

%% Definitions

voi=BVQXfile([path_voi,voiname],'please select voi file');

[smallest_NVox, smallest_mask] = min(extractfield(voi.VOI, 'NrOfVoxels')); % find the smallest mask so we  can equalise
                                                                           % number of voxels entered into svm later
if nargin<3
    vtc=BVQXfile([path_vtc, '*.vtc'],'please select vtc file on which voi was defined');
else
    vtc=BVQXfile([path_vtc,vtc1]);
end
bbx = vtc.BoundingBox.BBox;
bbx(2,:) = bbx(2,:);
res=vtc.BoundingBox.ResXYZ;

%% Initialize mask
mask1=zeros([vtc.BoundingBox.DimXYZ,length(voi.VOI)]);

%% Main loop

fprintf('Processing %s\n', sub)

for msk=indx
    fprintf('Processing %s (Mask %d of %d)\n', voi.VOI(msk).Name, msk, length(voi.VOI))
        
    mask=zeros(vtc.BoundingBox.DimXYZ);
    clear voi_coords
    voi_coords=voi.VOI(msk).Voxels;
    if strcmp(voi.ReferenceSpace,'TAL')
        %voi_coords1=round(bvcoordconv(voi_coords, 'tal2bvc',vtc.BoundingBox));
        voi_coords=128-voi_coords;
        tmp=voi_coords;
        voi_coords(:,1:2)=tmp(:,2:3);
        voi_coords(:,3)=tmp(:,1);
        voi_coords1 = round([(voi_coords(:, 1) - bbx(1,1)) ./ res(1) + 1, ...
                             (voi_coords(:, 2) - bbx(1,2)) ./ res(2) + 1, ...
                             (voi_coords(:, 3) - bbx(1,3)) ./ res(3) + 1]);

        for vxl=1:length(voi_coords)
            mask(voi_coords1(vxl,1),voi_coords1(vxl,2),voi_coords1(vxl,3))=1;

        end
    elseif strcmp(voi.ReferenceSpace,'TAL')==0 && max(voi_coords(:,1))>vtc.BoundingBox.DimXYZ(1) || max(voi_coords(:,2))>vtc.BoundingBox.DimXYZ(2) || max(voi_coords(:,3))>vtc.BoundingBox.DimXYZ(3)
        mbx=repmat(bbx(1,:),length(voi_coords),1);
        voi_coords=voi_coords-mbx;
        voi_coords=round(   [voi_coords(:, 1) ./ res(1) + 1, ...
                             voi_coords(:, 2) ./ res(2) + 1, ...
                             voi_coords(:, 3) ./ res(3) + 1]);
                       
        if max(voi_coords(:,1))>vtc.BoundingBox.DimXYZ(1) || max(voi_coords(:,2))>vtc.BoundingBox.DimXYZ(2) || max(voi_coords(:,3))>vtc.BoundingBox.DimXYZ(3)
            
            % this indicates that the VOI is outside of the VTC bounds, so
            % we'll alert the user and then just erase the VOI voxels that
            % end up outside. -ATM 05/08/15
            disp('voi and vtc dimension mismatch - will cut voi accordingly')
        end
        if min(voi_coords(:,1))<=0 || min(voi_coords(:,2))<=0 || min(voi_coords(:,3))<=0
            disp('voi and vtc dimension mismatch - will cut voi accordingly')
            voi_coords = voi_coords(find(min(voi_coords')>0), :);   
%             error ('negative or zero coordinates in native or acpc space')
        end
        
        % now do the 
        old_progress = 0;
        for vxl=1:length(voi_coords)
            % a simple if statement for this... would be faster with
            % indexing
            
            progress = round(vxl./length(voi_coords).*100);
            if progress > old_progress
                fprintf('%d Percent...\n', round(vxl./length(voi_coords).*100))
                old_progress = progress;
            end
            
            if voi_coords(vxl,1)<=vtc.BoundingBox.DimXYZ(1) &&...
               voi_coords(vxl,2)<=vtc.BoundingBox.DimXYZ(2) &&...
               voi_coords(vxl,3)<=vtc.BoundingBox.DimXYZ(3)
                mask(voi_coords(vxl,1),voi_coords(vxl,2),voi_coords(vxl,3))=1;
            end
        end
    elseif strcmp(voi.ReferenceSpace,'TAL')==0
        voi_coords=round(voi_coords ./ res(1) +1);
        for vxl=1:length(voi_coords)
            mask(voi_coords(vxl,1),voi_coords(vxl,2),voi_coords(vxl,3))=1;
        end
    end

    %     if sum(size(mask)~=vtc.BoundingBox.DimXYZ)~=0
    %         error('size of mask doenst match voi size')
    %     end
    mask(vtc.BoundingBox.DimXYZ(1)+1:end,:,:)=[];
    mask(:,vtc.BoundingBox.DimXYZ(2)+1:end,:)=[];
    mask(:,:,vtc.BoundingBox.DimXYZ(3)+1:end)=[];
    mask1(:,:,:,msk)=mask;
    
    % make it logical
    mask1=mask1==1;
           
    MASK = BVQXfile('new:msk');
    MASK.XStart = vtc.XStart;
    MASK.YStart = vtc.YStart;
    MASK.ZStart = vtc.ZStart;
    MASK.ZEnd = vtc.ZEnd;
    MASK.YEnd = vtc.YEnd;
    MASK.XEnd = vtc.XEnd;
    MASK.Resolution = vtc.Resolution;
    MASK.Mask = mask1(:,:,:,msk);
    MASK.SaveAs(sprintf('%s%s_%s_Mask.msk', path_voi, sub, voi.VOI(msk).Name));

    if nargin<7 || pRF_Mask == 0
        pRF_Mask = length(voi.VOI)+1;
    end
    
    if msk < pRF_Mask
        outV.voi_names{msk} = voi.VOI(msk);
        outV.MASK.XStart = vtc.XStart;
        outV.MASK.YStart = vtc.YStart;
        outV.MASK.ZStart = vtc.ZStart;
        outV.MASK.ZEnd = vtc.ZEnd;
        outV.MASK.YEnd = vtc.YEnd;
        outV.MASK.XEnd = vtc.XEnd; 
        outV.MASK.Resolution = vtc.Resolution;
        outV.MASK.Size = size(MASK.Mask);
    end
    
    outV.smallest_vmr_mask = [smallest_mask, smallest_NVox];
    outV.voiname=voi.VOI(msk).Name; %sanity check
end
