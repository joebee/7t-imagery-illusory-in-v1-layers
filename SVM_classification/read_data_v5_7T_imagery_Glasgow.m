function [outD]=read_data_v5_7T_imagery_Glasgow(sub,sdate,voi_indx, voi_appen)

% code from Muckli lab with adjustments for this project

% sub = subject code
% sdate = date string for subject folder
% voi_indx = refers to indx of relevant voi or poi
% voi_appen = e.g. '_peripheral', ''

% Version 5 some updates:

fileNames=getFileInfo3_7T_imagery_Glasgow(sdate,sub, voi_appen);  %% getFileInfo

zBetas=0;       % zscore betas after GLM voxelwise

zTimeSeries=1;      %% this is critical - if zTimeSeries==1, then betas should
                    % be comparable to those computed by BV with zNorm on
                    % (correct to 2 decimal places)
                    % otherwise betas are highly correlated but on diff scale

                    
% load in BV toolbox
addpath(genpath('/PATH_TO/BVQXtools_v08b'));

% sort file names from input
dirName=fileNames.dir_name;
prtdirName=fileNames.prt_dir_name;
voi_dir_name=fileNames.voi_dir_name;
voi_vtc_name=fileNames.voi_vtc_name;
locName=fileNames.loc_name;
dataName=fileNames.data_name; % mtc or vtc files
dmName=fileNames.dm_name;
condLocs=fileNames.cond_locs;
p=fileNames.pars;  %% nClass and nVols
subject=fileNames.subject;
typeS=2%fileNames.type; % mtc or vtc analysis 
prtNames=fileNames.prtNames;

% some parameters
nRuns=length(dataName);
nVols=p(1);
nPreds=p(2);
nTrials=p(3);
nPerRun=p(4);
nPredsSimple=p(5);
TR=p(6);


if(typeS==1)
    % load in POI FILE
    poi=BVQXfile([dirName locName]);
    nPOI=poi.NrOfPOIs;  
    locsV=[];

    % note the **unique** function here
    if(nPOI==1)
        locsV=unique(poi.POI.Vertices);
    else
        locsV=unique(poi.POI(indx).Vertices);  
    end
elseif(typeS==2)
    % load in Voi file
    voi=BVQXfile(sprintf('%s%s',dirName, locName));
    nVoi=voi.NrOfVOIs;
   [bbx,maskDims,outV]=voi2msk_ACPC(subject, voi_dir_name, voi_dir_name, locName, fileNames.voi_vtc_name, voi_indx); 


     locsV = maskDims(:,:,:,voi_indx);
     locsV == locsV ==1;
     locsV = find(locsV);

end



nV=length(locsV); % nVox or nVerts
Data=zeros(nVols,nV,nRuns);
DM=zeros(nVols,nPreds,nRuns);
DMnew=zeros(nVols,nPreds,nRuns);
betas=zeros(nPreds-1,nV,nRuns);  %% minus one for mean confound
tvals=zeros(size(betas));
snames{1}='mtc'; snames{2}='vtc';
% main loop
for r=1:nRuns

    fprintf('Processing Run: %d\n', r);
    % load in DATA

    main=BVQXfile([dirName dataName{r}]);
    
    
    if(typeS==1)        
        Data(:,:,r)=double(main.MTCData(:,locsV));  %% get VERTS timecourses from POI file       
    elseif(typeS==2)  
        for i=1:nVols   %% get timecourse from voxels vtc
            tmp=[];
            tmp=squeeze(main.VTCData(i,:,:,:));  %% one volume all voxels
            Data(i,:,r)=tmp(locsV);           
        end      
    end
    
   
    if(length(find(Data(:,:,r)==0))>0)
        %error(sprintf('Zeros in %s data: requires further thought!',snames{typeS}));
    end
 
    % load in DESIGN MATRIX file
    dmTmp=BVQXfile([dirName dmName{r}]);
    DMnew(:,:,r)=[dmTmp.SDMMatrix(:,1:nPreds)]; %1:33
    DM(:,1:nPreds,:)=DMnew(:,1:nPreds,:);

    if(size(DM,1)~=size(Data,1))
        error(sprintf('Design Matrix and %s data volume number does not match',snames{typeS}));
    end



    %% PERFORM GLM COMPUTATION - SINGLE TRIAL / BLOCK COMPUTATION
    %    ----- GLM   -----
    

    % glm performed single trial / block, not deconvolved (can't be)
    % importantly I am zscoring the timeseries here before running GLM
    % in order to obtain comparable values to BV output
    [out,out2]=compute_glm2(Data(:,:,r), DM(:,:,r), zTimeSeries,zBetas);

    betas(:,:,r)=out(1:nPreds-1,:);  %% remove last beta, mean confound
    tvals(:,:,r)=out2(1:nPreds-1,:);  %% t values
    
end   %% end loop across runs
    
%% post processing
% added in (6/9/11) to remove any voxels where mean BOLD signal change is < 100
% for comparability to BVQX main program method (applies to VTC data only at present)
% if(typeS==2)  %% ie VTCs
% compute mean BOLD signal per voxel
if(typeS==2)  %% 

    pz=[];
    for r=1:size(Data,3)
        for j=1:size(Data,2)
            pz(j,r)=mean(Data(:,j,r));   
        end
    end

    % find those under 100 and concatenate across runs
    py=[]; list=[];
    for r=1:size(Data,3)
        py{r}=find(pz(:,r)<=100); 

        if(~isempty(py{r}))
            list=[list py{r}'];
        end
    end
    list=unique(list);

    nVoxDat=[];
    % remove those voxels
    if(~isempty(list))
        betas(:,list,:)=[];
        tvals(:,list,:)=[];
        nV=size(betas,2);
        Data2=Data;
        Data(:,list,:)=[];

        % update locs
        locsV(list)=[];

        % write new updated mask  -for the purists- a few voxels out
        mask=zeros(main.BoundingBox.DimXYZ);
        mask(locsV)=1;
        outname=sprintf('%s_updated%i_%i.vmp',fileNames.subject,voi_indx,nV);

        nVoxDat=nV;
    end

end



   
%% GET THE DESIGN SEQUENCE AND PARSE BETAS CONDITION-WISE
% FWS change this to the modern style which is easier to understand and run
% etc --- and then write a simple run classifier func

nMPreds=nPreds-1; %nPreds-(nMapping+1); %number of preds - (fix + mapping)??
[nBetas,nVoxB,nRunsB]=size(betas);
stackD=zeros((nMPreds)*nRuns,nVoxB);
stackCM=zeros((nMPreds) *nRuns, 2);

k=1; l=nMPreds;
for r=1:nRunsB
    
    % grab behaviour
    seq=load([prtdirName condLocs{r}]);
    seq=seq-1; % subtract -1 as the first condition (baseline fixation, black background) is omitted
    seq=seq';  %% flip for my data (in rows)
    seq=seq(:);  %%concatenate into one trial vector of conditions codes

    % stack data and condition codes
    stackD(k:l,:)=betas(1:nMPreds,:,r);
    stackCM(k:l,1:2)=[seq r*ones(nMPreds,1)];
    k=k+nMPreds; l=l+nMPreds;
end


%% PUT IN STRUCTURES for OUTPUTTING
p(1)=nTrials;
p(2)=length(fileNames.preds2corr);
p(3)=nPerRun;
p(4)=nV;  
p(5)=nRuns;
p(6)=nVols;

s=cell(1,4);
s{1,1}=subject;
s{1,2}=dirName;         %% where to save any output files
s{1,3}=p;               %% useful parameters
s{1,4}=locsV;           %% all verts in POI / voxels in voi
s{1,5}=fileNames;   



outD=[];
outD.stackB=stackD;     %% betas stacked -this way very easy to do classification
outD.stackCM=stackCM;   %% code matrix (conditions, runs) labelling

outD.betas=betas;       %% betas by runs
%outD.tvalsC=tvalsC;     %% t vals by condition
outD.data=Data;      %% raw vertice/voxel timecourses for POI/VTC, note it may have more voxels than betas/ts (some may be removed if mean bold < 100)
outD.DM=DM;             %% design matrices
outD.S=s;               %% useful parameters
outD.p=p;
outD.voi=outV;
outD.voiname=outV.voiname; 
outD.list_removed_vox=list; %voxels that have been removed from ROI because  mean BOLD<100







