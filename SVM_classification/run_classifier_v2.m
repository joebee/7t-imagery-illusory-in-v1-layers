function [out]=run_classifier_v2(stackD, labelV, runV,normFlag, perm)

% function [stats_svm, stats_svmAv, pc]=run_classifier(data, labels, runLabels)
% FWS 08/06/2012 CCNI GLASGOW UNIVERSITY
% if you use this code for analysis - please cite Smith & Muckli 2010 PNAS
% Non-stimulated early visual areas carry information about surrounding context
% PNAS, 107 (46) 20099-20103.
% adjustments made for this project

% data should be nTrials OR nBlocks (rows) by nVoxels (cols)
% labels should be nTrials OR nBlocks by 1
% XV labels should be same as labels (coding cross-validation sets, usually
% RUNS or leave one trial of each set out)

% further developed -v2 - 30/11/12 CCNi Glasgow --- to re-incorportate
% permutation test option (takes around 23 secs to do 1000 iterations)
% code will then estimate observed performance, and a permutation distribution
% for both single trials and average decoding, as well as p values
% and corresponding empirical chance levels

% add svm to path
rmpath(genpath('/PATH_TO/BVQXtools_v08b')); %to avoid conflict with libsvm (functions with same name)
addpath(genpath('/PATH_TO/libsvm-mat-2.86-1')); % SVM toolbox


% find number of trials / voxels
[nT,nV]=size(stackD);

% find number of classes / trials per class
nC=length(unique(labelV));  % number of classes
ulabel=unique(labelV); %% the actual labels
tCount=histc(labelV,ulabel(1:nC));

% find nRuns
nR=length(unique(runV));
runs=unique(runV);  
rCount=histc(runV, 1:nR); 

% show trials by voxels matrix - the magic matrix of mvpa
%figure, imagesc(stackD)


%normFlag=1;  % choose SVM normalization method (1 is recommended, puts between -1 and 1 per feature)

% make data noise

if(perm) %perm=0 in this project (no permutation on within-subject level)
    nIts=1000; % include the real case
    permPC=zeros(nIts,nR,2);
    rand('state', sum(100*clock));
else
    nIts=1;
end

for xx=1:nIts  %% permutation loop

    gnb_class=[]; svm_class=[];
    % which XV to use - leave one run out (cross-validation)
    for i=1:nR


        r=runs(i);  % a little switch
        % r is run to be held out for testing this cycle

        % define XV indices
        teIndx=find(runV==r); % test index
        trIndx=find(runV~=r); % train index

        % define train/test data
        train=stackD(trIndx,:);
        test=stackD(teIndx,:);

        % define train / test labels
        teLabels(:,i)=labelV(teIndx);
        trLabels=labelV(trIndx);

        % do permutation of labels? 
        if(xx<nIts && perm)
            % do stratified label scrambling of training labels within runs
            [trLabels]=permlabels(trLabels,runV(trIndx));
        end


        %---------------% run gnb classifier -- don't need explicit 
        %normalization here - it is implicit in the definition
        [gnb_class(:,i),err,post,logp,coef]=classify(test,train,trLabels,'diagLinear');

        %---------------% run SVM classifier (what we're interested in in this project)
        [train,test]=svm_scale_data_mvpaC(train, test, normFlag);  % put each voxel in 1 to -1 range

        % train SVM
        svm_model=svmtrain(trLabels, train,'-t 0 -c 1');    % -t 0 = linear SVM, -c 1 = cost value of 1
        a = extract_activations_libsvm(svm_model, train); % computes which voxel contain the task-related information (in machine learning also termed 'activation')

        % test SVM
        [svm_class(:,i),accuracy,dec]=svmpredict(teLabels(:,i),test,svm_model);

        % test SVM on average patterns in independent test data
        for ii=1:nC
            avL=find(teLabels(:,i)==ulabel(ii));
            avT(ii,:)=mean(test(avL,:));  %% mean across test trials per condition
        end
        [svm_classAv(:,i),acc2,dec2]=svmpredict(ulabel,avT,svm_model);

    %    %---------------% compute classification performance
         pc(i,1)=length(find(gnb_class(:,i)==teLabels(:,i))) / length(teLabels(:,i)); 
         pc(i,2)=length(find(svm_class(:,i)==teLabels(:,i))) / length(teLabels(:,i));
         pc(i,3)=length(find(svm_classAv(:,i)==ulabel)) / length(ulabel); %classification values of interest in project (one per fold)
    end

    % make nice stats structure

    % following not used in this project
%     stats_svm=deriveCM_pvals(svm_class,teLabels,nC); % this is the important one
%     stats_gnb=deriveCM_pvals(gnb_class,teLabels,nC);
%     stats_svmAv=deriveCM_pvals(svm_classAv, repmat(ulabel,1, nR), nC);
%     
%     if(perm)
%         % keep track of the permuted results
%         permPC(xx,:,1)=stats_svm.CorrRun';
%         permPC(xx,:,2)=stats_svmAv.CorrRun';
%     end
    
end  %% 

% figure, bar(pc), legend('gnb - single trial','svm - single trial','svm - average');
% hold, plot(0:.01:nR+1,1/nC,'LineWidth',4)


out.pc=pc;
out.a=a; %contains which voxels contain the task-related information;
out.train=train; %the normalized data used for training the SVM
out.svm_model=svm_model;
out.pair_labels=ulabel;

%commented (not used in this project)
% if(perm)  %% if we are doing permutation test, store a few more things
%     out.permPC=permPC;
%     
%     for i=1:2  % single block/trial or average pattern decoding performance
%        mD(:,i)=mean(permPC(:,:,i),2); 
%        figure, histfit(mD(:,i))  % the permutation distribution
%        
%        obs(i)=mean(permPC(end,:,i));
%        permPval(i)=length(find(mD(:,i)>=obs(i)))./numel(mD(:,i));  %% one sided
%        empChanceLevel(i)=mean(mD(:,i));
%     end
%     
%     out.mD=mD;  %% the permutation distributions (should be centered on chance)
%     out.obs=obs; %% the observed accuracies from the real data
%     out.permPval=permPval;  % p values from permutation distributions
%     out.empChanceLevel=empChanceLevel;  % empirical chance levels (these can be used to correct obs perf before second level analyses across groups)
% 
% end



