function [input_file_info]=getFileInfo3_imagery_Glasgow(sdate,sub, voi_appen)
% code from Muckli lab with adjustments for this project

%  path to the files
root_dir='/ROOT_DIR';
dir_name=sprintf('%s/analysis/BV_analysis/%s_%s/', root_dir, sdate,sub); %fMRI data name

prt_dir_name=sprintf('%s/stimulation/participants_mat_files/%s/', root_dir, sub); %prt directory (if different)
voi_dir_name=sprintf('%s/analysis/BV_analysis/%s_%s/',root_dir, sdate, sub); %VOI directory (if different)
voi_vtc_name=sprintf('%s_RetMaps_PolarAngle_SCCTBL_3DMCTS_LTR_THPGLMF7c_ACPC.vtc',sub); %fMRI file name (can also be from retintopic mapping...only used for voi2msk_ACPC.m)


% ROI file name
loc_name=sprintf('%s_V1_imagery_pRF%s.voi',sub, voi_appen);%

%%% important parameters
Runs=[1 2 3 4 5 6 ];
nRuns=length(Runs);
nConditions=5; %
nMapping=2; %number of mapping conditions
nVols=272; % number of volumes, i.e. time in TR
nPerRun=2;   %%(each condition in two colours, and each mapping shown twice
% nPreds = number of trials per run plus 1;  includes the mapping conditions
nPreds=nConditions * nPerRun * 3 + nMapping*nPerRun+1; %multiply by 3 as each condition*colour shown 3 times per run
nPredsSimple=nConditions* 3 +nMapping; %no constant added here;
preds2corr=1:(nConditions+nMapping);
nTrials=nConditions * nPerRun * 3 + nMapping *2; %34
TR = 2000;  % in msec so TR 1 = 1000msec


% subject data file names 
for r=1:nRuns
    
    %if fMRI runs have been re-aligned to improve func-to-func or
    %func-to-anat alignment, make sure correct
    %file is loaded
    data_name{r}=sprintf('%s_imagery_NCS_run%d_SCCTBL_3DMCTS_LTR_THPGLMF5c_ACPC.vtc',sub,Runs(r)); %fMRI data    
    dm_name{r}=sprintf('%s_imagery_NCS_run%d_SCCTBL_3DMCTS_LTR_THPGLMF5c_ACPC_SingleTrials.sdm',sub,Runs(r));%design matrix
    cond_locs_name{r}=sprintf('%s_%d_7Tesla_imagery_Glasgow.txt', sub, Runs(r)); %condition sequence txt file
    
    % the standard condition based prt names
    prtNames{r}=sprintf('%s_%d_7Tesla_imagery_Glasgow.prt',sub,Runs(r)); %BV prt file

end


% put into structure for easy passing
input_file_info=[];
pars(1)=nVols;
pars(2)=nPreds;  %% remember to add 1 for the constant column
pars(3)=nTrials;  
pars(4)=nPerRun;
pars(5)=nPredsSimple;
pars(6)=TR;
pars(7)=nMapping;

input_file_info.data_name=data_name;
input_file_info.dir_name=dir_name;
input_file_info.prt_dir_name=prt_dir_name;
input_file_info.voi_dir_name=voi_dir_name;
input_file_info.voi_vtc_name=voi_vtc_name;
input_file_info.loc_name=loc_name;
input_file_info.dm_name=dm_name;

input_file_info.cond_locs=cond_locs_name;  
input_file_info.pars=pars;
input_file_info.subject=sub;
input_file_info.prtNames=prtNames;
input_file_info.preds2corr=preds2corr;


