# README

This is the analysis code we used for the Bergmann et al. publication "Cortical depth profiles in primary visual cortex for illusory and imaginary experiences".
* Source Data can be found in the folder linear_mixed_model ('Source_Data.xlsx').

## Toolboxes/functions required:
MATLAB:
* knkutils (Kendrick Kay's knkutils (https://github.com/kendrickkay/knkutils)
* BVQX toolbox (version used v08b); alternatively Neuroelf v.09c (code would need to be adjusted accordingly)
* LibSVM (v2.86), Chang & Lin (2011)
R with nlme and report libraries

MATLAB codes require data to be in BrainVoyager format (BrainVoyager 20.6)

R (with RStudio) [R version: 4.2.1]:
* readxl
* nlme
* report

# Description 
## population receptive field (pRF) mapping
Run pRF code in sequence indicated by file names;visual field areas of interest of the two experiments are found as mat files in respective subfolders. Note that in experiment 1, 4 participants saw slightly bigger stimuli (stOff=280 in file name), and hence, the visual areas of interest are slightly bigger (stOff=280 in file name) - see additionalNotes in participants.tsv in data repository.
stackedRetmapStim_imagery_NCS.mat is needed in Step 1. It contains two matrices, one for polar angle mapping and one for eccentricity mapping. 
Here, The 216x216 frames (downscaled from pixels to save time) of the retinotopic mapping stimulation have been vectorized to form one column with a length of 46656.  
Each  of the 'image frames x timepoint (in TR)' matrices hence have a size of : 46656x396 for Polar Angle and 46656x268 for Eccentricity.

In order to visualize a 2D-frame from a given timepoint, you need to reshape a column by taking the square root of all pixels 46656 (which is 216). 
E.g. visualize frame in matlab for timepoint 40 of Eccentricity mapping:

~~~
 imagesc(reshape(EccStim(:,40),sqrt(size(EccStim,1)),sqrt(size(EccStim,1))))
~~~

* Step1_Create_PredTimecourses.m: creates gaussian pRF models and calculates their predicted timecourses given the retinotopic stimulation
* Step2_Define_pRFs.m: computes which of the pRF models explains a voxel's activity best (provide mask of area, e.g. V1 )
* Step3_pRF_plot.m: plots pRF models,  computes which pRF models overlap with visual field area of interest, creates mask containing all voxels whose pRF model is in visual field area of interest (thresholded by r^2 to filter out noise)

## SVM classification
Computes support vector machine classification for the five conditions across 6 cortical depth layers of the region of interest (e.g. central or peripheral region in Experiment 1).
Run with subsRunTemplate_7T_imagery_Glasgow.m. 
fMRI Data, name of region of interest (voi with 6 cortical depths), design matrices and condition label file (containing the sequence of the condition trials) need to be provided in getFileInfo3_7T_imagery_Glasgow.m.
Output is one mat-file per subject. Cross-validated Classification accuracy averaged across the folds is found in subRes(:,:,2), where the first dimension is the condition pair index, and the second dimension the cortical depth index. 

## Linear mixed model
Fits a linear mixed model to predict decoding accuracy with depth, experiment and stimulus condition (mental imagery and illusory perception). The model predictions are made for both conditions across experiments and are plotted alongside the experimental data. The last plot is shown in Figure 3 of the manuscript. The dataframes containing the subjects' decoding performance data are provided in the source data file 'Source_Data.xlsx' (sheet 'Figure_3').

## Authors
The population receptive field mapping code was written by the Muckli lab and was adapted to this project by Johanna Bergmann. 
The core code for SVM classification is a wrap around for the LibSVM toolbox and was originally written by Fraser Smith (Smith & Muckli 2010 PNAS). It was adapted to this project by Johanna Bergmann.
The linear mixed model code to predict decoding accuracy across the two experiments was contributed by Clement Abbatecola.

