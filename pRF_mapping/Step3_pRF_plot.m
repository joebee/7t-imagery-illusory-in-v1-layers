% Computes which voxels' pRF are located within visual area of interest
% code by Muckli lab
%%
clear all
close all
clc

load('/PATH_TO/visual_area_of_interest.mat'); %binarized 'window' of visual target area
  
directory= ['/PATH_TO_FMRI_DATA/' ]; % path to subjects' fMRI data
directory_PRF='/PATH_TO/Defined_pRFs/'; %path to subject-specific voxel pRF models

% paths to toolboxes
addpath(genpath('/PATH_TO/knkutils')); %Kendrick Kay's knkutils (https://github.com/kendrickkay/knkutils)
addpath(genpath('/PATH_TO/BVQXtools_v08b')); %alternatively, use NeuroElf v.09c 

Subject_Info = {'SUBJECT_ID'};
anat_dir= 'SCANDATE_SUBJECTID/'; %where to find the mask of V1

appen='_target_pRF';% appendix for saving files e.g. '_peripheral' or '_central' 


%% also  don't forget to define the stimdims further down

OverlapPercentageAmount=[1]; % How much overlap should the PRFs have with the visual area of interest (up to 1 SD of the Gaussian)? enter as ratio, i.e. 1=100%
rsqrdcut_list=[0.2]; %r-squared threshold to filter out noisy voxels Experiment1: 0.2 Experiment 2: 0.1;
%enter several values if you want to check multiple thresholds as sanity check

for r=1:length(rsqrdcut_list)
    
rsqrdcut = rsqrdcut_list(r); 
%%
area='_V1';
used_mask=area;

for Subject_Index = 1:size(Subject_Info,2)
    close all
    
%% load subject-specific pRF file
    load([directory_PRF Subject_Info{Subject_Index} '_Defined_PRFs' area '_imagery_NCS.mat']); 
    disp('[size(best_model, 1),  size(best_model, 1)/4]')
    [size(best_model, 1),  size(best_model, 1)/4]

    %% get the correct pixperVA calc
    stimdim = 216;%(scaled down for speed) 
    stimdim_mm = 320;%230;%348; % this is the vertical screen dimension in mm
    screendist = 959;%1355; %1025 distance to screen in mm ([eye to mirror] + [mirror to screen])
    TR = 2; % fmri repeat time
    stimrad = stimdim / 2;
    stimrad_mm = stimdim_mm / 2; % stimulus radius in mm
    visrad = atand( stimrad_mm / screendist ); % stim radius in visual degrees
    pixperVA = stimrad / visrad;
    centre_fix_x=stimrad;
    centre_fix_y=stimrad;
    
    

    %% plot all voxels' pRF models above threshold 

    figure,
    plot(gind(best_model(high_rsq>rsqrdcut),2),gind(best_model(high_rsq>rsqrdcut),3),'.'),
    axis([0 stimdim 0 stimdim]),
    axis square


    %% draw circles around them (requires drawellipse from knkutils)
      
    for cc = find(high_rsq>rsqrdcut)' % 1:length(best_model),
        drawellipse(gind(best_model(cc),2),gind(best_model(cc),3),0,...
            pixperVA*gind(best_model(cc),1),pixperVA*gind(best_model(cc),1));
    end
    title(sprintf('%s whole field pRFs RSq%s', Subject_Info{1}, num2str(rsqrdcut)))
    saveas(gcf,sprintf('%s/%s%s_pRFsImage_RSq%s.png', directory_PRF, Subject_Info{Subject_Index}, used_mask, num2str(rsqrdcut))); %
    
    %% plot eccentricity vs pRF size (sanity check)
       
     for cc = find(high_rsq>rsqrdcut)' % 1:length(best_model),
        ecc_dist(cc) = ...
            sqrt((gind(best_model(cc),2)-centre_fix_x)^2 + ...
            (gind(best_model(cc),3)-centre_fix_y)^2);
    end
    
    % actually plot it
    figure,  
    plot(ecc_dist(high_rsq>rsqrdcut)/pixperVA,gind(best_model(high_rsq>rsqrdcut),1),'.')
    title(sprintf('%s eccentricity vs pRF size RSq%s', Subject_Info{1}, num2str(rsqrdcut)))
    xlabel('Eccentricity'); ylabel('pRF size');
    saveas(gcf,sprintf('%s/%s_pRFsEccentricity_RSq%s.png', directory_PRF, Subject_Info{Subject_Index}, num2str(rsqrdcut))) %
    
    %% Calculate voxels for Classifier
    Gridsize = stimdim;
    
    for Targets = 1% only 1 , as computed separately
        if Targets == 1

                TargetV1Mask=png_new; 
        elseif Targets == 2  %JB: not used
            
            [rr cc] = meshgrid(1:Gridsize); % rr and cc are x and y
            TargetV1Mask = zeros(stimdim,stimdim);
           
            TargetV1Mask((size(TargetV1Mask,2)/2)-33:(size(TargetV1Mask,2)/2)+33, (size(TargetV1Mask,2)/2)-33:(size(TargetV1Mask,2)/2)+33) = 1;
            trim = fliplr(triu(ones(stimdim), -30));
            TargetV1Mask = TargetV1Mask.*trim;
            TargetV1Mask = flipud(TargetV1Mask);
        end
        TargetV1Mask  = logical(TargetV1Mask);
        
        figure, imshow(flipud(TargetV1Mask)) % should be flipped..

        axis on
        hold on

        
        pRFPassVoxels = [];
        
        %% Plot only those pRF that fall in the area of interest 
        % also see OverlapPercentageAmount to adjust overlap
        % Have logical area of interest
        
        counter = 0;
        plotcount = 0;
        for i=1:length(OverlapPercentageAmount)
            OverlapPercentage=OverlapPercentageAmount(i);
           
            for ccc = find(high_rsq>rsqrdcut)' % 1:length(best_model),
                counter = counter + 1;
                
                centrex = gind(best_model(ccc),2); % x coords of voxel's pRF
                centrey = gind(best_model(ccc),3); % y coords of voxel's pRF
                RadiusPixels = pixperVA*gind(best_model(ccc),1); % radius of voxel's pRF
                
                [rr cc] = meshgrid(1:Gridsize); % rr and cc are x and y of the whole screen, rr takes on values of 1:Gridsize along the x direction, cc values of 1: Gridsize along the y direction
                pRF_tmp = sqrt((rr-centrex).^2+(cc-centrey).^2)<=RadiusPixels; % <= turns it into a logical
                
                
                %% determine pRFs within visual area of interest
                Overlap_pRF_V1Mask=TargetV1Mask(pRF_tmp);                 
                RequiredOverlap=sum(sum(pRF_tmp)).*OverlapPercentage;
                
                if sum(Overlap_pRF_V1Mask)>=RequiredOverlap
                    pRFPassVoxels=[pRFPassVoxels;ccc];                    
                end
            end
            
            figure,
            plot(gind(best_model(pRFPassVoxels),2),gind(best_model(pRFPassVoxels),3),'.'),
            axis([0 stimdim 0 stimdim]),
            axis square
            % set(gca,'YDir','reverse');
            for cc = pRFPassVoxels'
                drawellipse(gind(best_model(cc),2),gind(best_model(cc),3),0,...
                    pixperVA*gind(best_model(cc),1),pixperVA*gind(best_model(cc),1));
            end
            title(sprintf('%s %s %s area RSq:%s RequiredOverlap:%s No. Voxels:%s', Subject_Info{1}, area, appen, num2str(rsqrdcut), num2str(OverlapPercentage), num2str(length(pRFPassVoxels))), 'interpreter', 'none')
            saveas(gcf,sprintf('%s/%s_%s_%s area_RSq%s_RequiredOverlap_%s.png', directory_PRF, Subject_Info{1}, area, appen, num2str(rsqrdcut), num2str(OverlapPercentage))) %

             
            
        
             for cc = 1:length(msk_fns), 
                bvqx_masks{cc} = BVQXfile([anat_path{:} msk_fns{cc}]);
                bvqx_masks{cc}.SaveAs([directory_PRF Subject_Info{1} '_sanity_check' used_mask '.msk']);


                                
                %% pRF voxels in V1: create mask for V1 voxels that represent visual area of interest
                %load areal mask
                rsqrdcut_str=num2str(rsqrdcut);
                OverlapPerc_str=num2str(OverlapPercentageAmount);
                Mask=BVQXfile([directory anat_dir Subject_Info{Subject_Index} used_mask '.msk']); % mask that pRFs were computed for
                Mask_tal_coords = Mask.Mask;
                Mask_tal_coords = find(Mask_tal_coords);
                Mask_tal_coords_pRF=Mask_tal_coords(pRFPassVoxels);
                copy_mask=Mask.Mask;
                Mask.Mask=zeros(size(copy_mask)); %create new mask that only contains zeros at first (same size as before)
                Mask.Mask(Mask_tal_coords_pRF)=240; %now assign all voxels that belong to pRF value of 240  (mask colour)         
                    
                if length(OverlapPerc_str) > 1
                    Mask.SaveAs([directory '/' anat_dir Subject_Info{Subject_Index} ...
                    '_' area appen '_RSq'  rsqrdcut_str(1,end-1:end) '_ReqOverlapPerc' OverlapPerc_str(1, end-1:end) '.msk']);
                else
                    Mask.SaveAs([directory '/' anat_dir Subject_Info{Subject_Index} ...
                    '_' area appen '_RSq'  rsqrdcut_str(1,end-1:end) '_ReqOverlapPerc' OverlapPerc_str(1, end) '.msk']);
                end
                
             end
        end
        

    end
end

savename=[directory_PRF '/' Subject_Info{1} used_mask appen '_thresh_' num2str(rsqrdcut) '_ReqOverlapPerc' OverlapPerc_str(1, end) '_pRFPassVoxels.mat'];
save (savename, 'pRFPassVoxels');
end
