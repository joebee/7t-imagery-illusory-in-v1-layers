%% Create_PredTimecourses_windows
%  this script makes predicted timecourses for prf mapping based on PA and
%  Ecc mapping from the Muckli lab. 
%%

% user-defined values
stimdim = 216; %768; 256; 
stimdim_mm = 320;%180; 348; % this is the vertical screen dimension in mm
screendist = 959; %990;%1025; % distance to screen in mm ([eye to mirror] + [mirror to screen])
min_sig = .1; % minimum pRF sigma to test
gaussreq = .5; % this is how much of each Gaussian window must be in the stimulus to be included
TR = 2;% % fmri repeat time

% paths
addpath(genpath('/PATH_TO/knkutils')); %Kendrick Kay's knkutils (https://github.com/kendrickkay/knkutils)
addpath(genpath('/PATH_TO/BVQXtools_v08b')); %alternatively, NeuroElf v.09c

load('/PATH_TO/stackedRetmapStim_imagery_NCS.mat'); %contains (binarized) version of retinotopic stimulation (in matrix form)


% name of saved output file
Pred_TC_Name = 'timeseries_PRF_for_7T_imagery_NCS_Glasgow_Polar_Ecc_only';

% measurement calculations
stimrad = stimdim / 2;
stimrad_mm = stimdim_mm / 2; % stimulus radius in mm
visrad = atand( stimrad_mm / screendist ); % stim radius in visual degrees
pixperVA = stimrad / visrad;

% calculate sigmas to use
logspacing = 24; % this is the number of sigma grids to create (log spaced)
sigmas = exp(linspace(log(min_sig),log(visrad),logspacing));

% hrf calculation (uses BVQXtools)
clear hrfunc %dumb BVQXtools requirement
hrfunc = hrf('twogamma',TR);

%% Need to combine the runs

% find the files (listed here)
stimfiles = { 'PAStim' 'EccStim'};


% read their lengths
ntp_ind = zeros(length(stimfiles),1);
for cc = 1:length(ntp_ind),
    ntp_ind(cc) = size(eval(stimfiles{cc}),2);
end
ntp = sum(ntp_ind);

%  preallocate stimuli size
tot_RM = sparse(zeros(size(eval(stimfiles{1}),1),sum(ntp_ind)));

% load them into the big array
for cc = 1:length(ntp_ind)
    st_ind = sum(ntp_ind(1:cc))-(ntp_ind(cc)-1);
    end_ind = sum(ntp_ind(1:cc));
    tot_RM(:,st_ind:end_ind) = eval(stimfiles{cc});
end

% clean up a little (get rid of stim files - they aren't needed from here
% on out)
clear *_Stim


%% Predefine all the Gaussian windows
%
%  We can do some  time-saving if we predefine the points that 
%  we'll be using and also make the assumption that no pRF can land 
%  outsize of the stimulus radius.

% make a stimulus circle

imcirc = sparse(double(makecircleimage(stimdim,stimrad)));
imcirc([1:32, 225:end],:) = 0;
imcirc = imcirc(:);

% define the grid size based on sigma size (separated by 1-sigma in 
% each direction)
gridx = floor((visrad * 2)./(sigmas));
gridl = (gridx .* sigmas) .* pixperVA; % grid length in pixels

% create the gridpoints along the side of the mesh and define the grid
linpts = cell(1,length(gridl));
gridpx = cell(1,length(gridl));
gridpy = cell(1,length(gridl));

for cc = 1:length(linpts),
    linpts{cc} = linspace( (stimdim - gridl(cc)) / 2, (gridl(cc) + stimdim) / 2,gridx(cc));
    [gridpx{cc}, gridpy{cc}] = meshgrid(linpts{cc});
    gridpx{cc} = gridpx{cc}(:);
    gridpy{cc} = gridpy{cc}(:);
end

% We'll define a speed-up for making the Gaussians at some point
% and pre-allocate for the timecourses
[xx,yy] = calcunitcoordinates(stimdim); % this is a speed-up for the makegaussian2d
ts = zeros(ntp,sum(gridx .^ 2));

% Next, index everything to negate the need for a double-main-loop
gind = [];
for g = 1:length(sigmas),
    gind = [gind; repmat(sigmas(g),length(gridpx{g}),1), gridpx{g} gridpy{g}];
end

% add a column to gind to mark whether it's a valid model
gind = [gind zeros(size(gind,1),1)];

% a counter and some stuff for a waitbar
cntr = 1;
totloop = size(gind,1);
ps = 0;


%% The main loop
tic

for cc = 1:size(gind,1),
    
    % define indices
    gp = pixperVA * gind(cc,1);
    px = gind(cc,2);
    py = gind(cc,3);
    
    % create the Gaussian window
    tmpgauss = makegaussian2d(stimdim,py,px,gp,gp,xx,yy);
    tmpgauss = tmpgauss(:);

    % define a Gaussian footprint to test if in the stimulus
    gaussfoot = 1.0 * (tmpgauss > 0.0001);
    
    % determine if we want to store it
    tmp_vol = imcirc' * gaussfoot; % dot prod of Gaussian footprint and stimulus (overlap)
    tmp_dis = pdist([gind(cc,2), gind(cc,3); stimrad stimrad]); %dist from center
    if  tmp_vol > gaussreq && tmp_dis < (stimrad - 1), % just barely smaller than stim radius
        ts(:,cc) = tot_RM' * tmpgauss;
        gind(cc,4) = 1;
    end
    
    % update count and report your progress
    prog = floor((cc/totloop) * 1000);
    
    if  prog ~= ps
        clc
        disp([num2str(prog/10) '% completed.']);
        ps = prog;
    end
    
end

toc

%% Get rid of bad models
ts = ts(:,logical(gind(:,4)));
gind = gind(logical(gind(:,4)),1:3);

%% Next, convolve the whole thing

% make time series cell structure
ts_conv = cell(length(ntp_ind),1);

for cc = 1:length(ntp_ind),
    st_ind = sum(ntp_ind(1:cc))-(ntp_ind(cc)-1);
    end_ind = sum(ntp_ind(1:cc));
    tmp_conv = conv2(ts(st_ind:end_ind,:),hrfunc);
    ts_conv{cc} = tmp_conv(1:ntp_ind(cc),:);
    %old way: %ts_conv(st_ind:end_ind,:) = tmp_conv(1:ntp_ind(cc),:);
    
    clear st_ind end_ind tmp_conv
end

%% Save it
save(sprintf('%s.mat', Pred_TC_Name),...
    'gind','ts','ts_conv','sigmas','ntp','ntp_ind','xx','yy','-v7.3');
