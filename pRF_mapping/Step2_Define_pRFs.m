%% Fit pRF models to data 
%  this script fits pRF models to individual data
%  code from the Muckli lab.
%% File definitions
clear all;
% file order (should be the same length as the vtc order)
% Polar Angle   -   1
% Eccentricity  -   2

run_order = [1 2]; 

% VTC names in a single cell structure 
vtc_fns = { 'SUBJECT_ID_RetMaps_PolarAngle_SCCTBL_3DMCTS_LTR_THPGLMF7c_ACPC_ALIGNED_to_vmr.vtc'...
            'SUBJECT_ID_RetMaps_Ecc_SCCTBL_3DMCTS_LTR_THPGLMF4c_ACPC_ALIGNED_to_vmr.vtc'...
   };

       
%  mask name (ROI needs to be provided as .msk)
Subject_Info={'SUBJECT_ID'};
Subject_date='ENTER_SCAN_DATE';
area='V1';
which_exp='imagery_NCS';
msk_fns={[Subject_Info{1} '_' area '.msk']};

%% Definitions
UsePoly = 0;
TR = 2;

%% Define paths & add toolboxes
path_dir = ['/PATH_TO_FMRI_DATA_FOLDER/' ]; %where all fMRI data are stored

% paths to toolboxes
addpath(genpath('/PATH_TO/knkutils')); %Kendrick Kay's knkutils (https://github.com/kendrickkay/knkutils)
addpath(genpath('/PATH_TO/BVQXtools_v08b')); %alternatively, use NeuroElf v.09c 

path = {fullfile(path_dir, [Subject_date '_' Subject_Info{:} '/']) ...
        }; %path to subject dir (SUBJECT-DATE_SUBJECT-ID)

anat_path = path; %adapt if anatomical is on different path (& only if ROI mask is stored there instead)

%% Load predicted timecourse models
load('/PATH_TO/timeseries_PRF_for_7T_imagery_NCS_Glasgow_Polar_Ecc_only.mat');

%% Process Masks
bvqx_masks = cell(length(msk_fns),1);
for cc = 1:length(msk_fns), 
    bvqx_masks{cc} = BVQXfile([anat_path{:} msk_fns{cc}]); 
    bvqx_masks{cc} = logical(bvqx_masks{cc}.Mask);
end

%% Process VTCs
bvqx_vtcs = cell(length(vtc_fns),1);
for cc = 1:length(vtc_fns), 
    bvqx_vtcs{cc} = BVQXfile([path{:} vtc_fns{cc}]); 
    bvqx_vtcs{cc} = bvqx_vtcs{cc}.VTCData;
end

%% Define Polynomials (these are separate for different runs)
% figure out how long each dataset is
nruns = length(bvqx_vtcs);
runs_length = zeros(nruns,1);
for cc = 1:nruns
    runs_length(cc) = size(bvqx_vtcs{cc},1);
end

% section commented by JB
if(UsePoly)
    % figure out number of degrees and construct ind. polynomials
    poly_p = floor(((ntp_ind(1) * TR) / 60) / 2); % standard 1/2 scan length = poly deg
    poly_e = floor(((ntp_ind(2) * TR) / 60) / 2); % same
    tmpp = constructpolynomialmatrix(ntp_ind(1),0:poly_p);
    tmpe = constructpolynomialmatrix(ntp_ind(2),0:poly_e); % this is used for bars, too
    
    % define poly degrees for runs
    tmprl = zeros(nruns,1);
    tmprl(runs_length == ntp_ind(1)) = poly_p + 1;
    tmprl(runs_length == ntp_ind(2)) = poly_e + 1;
        
    % make blank matrix
    polymat = zeros(sum(runs_length),sum(tmprl));
    
    for cc = 1:nruns
        tmpi = sum(runs_length(1:cc)) - (runs_length(cc) - 1);
        tmpi2 = sum(runs_length(1:cc));
        tmpj = sum(tmprl(1:cc)) - (tmprl(cc) - 1);
        tmpj2 = sum(tmprl(1:cc));
        if tmprl(cc) == poly_p + 1
            polymat(tmpi:tmpi2,tmpj:tmpj2) = tmpp;
        else
            polymat(tmpi:tmpi2,tmpj:tmpj2) = tmpe;
        end
    end
else % this just makes a standard confound matrix
    polymat = zeros(sum(runs_length),nruns);
    tmprl = zeros(nruns,1);
    
    for cc = 1:nruns
        tmpi = sum(runs_length(1:cc)) - (runs_length(cc) - 1);
        tmpi2 = sum(runs_length(1:cc));
        polymat(tmpi:tmpi2,cc) = ones(length(tmpi:tmpi2),1);

    end
end

%% Run the models and record r^2 values

% Make the time series matrix (there has to be an elegant way to do this...)
tmp_ts = zeros(sum(runs_length),sum(bvqx_masks{1}(:)));
for cc = 1:nruns
    tmpi = sum(runs_length(1:cc)) - (runs_length(cc) - 1);
    tmpi2 = sum(runs_length(1:cc));
    tmp_ts(tmpi:tmpi2,:) = bvqx_vtcs{cc}(:,bvqx_masks{1});
end

% a counter and some stuff for progress
totloop = size(ts_conv{1,1},2);
tickmark = zeros(1,100);
ps = 0;

r_sq = zeros(size(tmp_ts,2),totloop);

tic % for measuring elapsed time
for cc = 1:totloop
    
    % define the model design matrix (includes polynomial confounds)
    tmp_mod = [];
    for dd = 1:length(run_order)
        tmp_mod = [tmp_mod; ts_conv{run_order(dd),1}(:,cc)];
    end
    
    if(sum(tmp_mod(:,1))==0) %if the tmp_mod is zero (because the visual portion is outside of the visual stim)
        r_sq(:,cc) = 0;
    else
        tmp_des = [tmp_mod polymat];

        % normalize the design
        tmp_des = tmp_des ./ repmat(max(tmp_des),sum(ntp_ind(run_order)),1);

        % run maximum likelihood
        tmp_bs = tmp_des \ tmp_ts; 
        
        tmp_fit_pts = tmp_des(:,1) * tmp_bs(1,:); % fit pred. timeseries
        tmp_fit_poly = tmp_des(:,2:end) * tmp_bs(2:end,:); % fit polynomials
        ts_filtered = tmp_ts - tmp_fit_poly; % filter data

        % r^2 value (pred. timeseries -> filtered data)
        ts_mu = repmat(mean(ts_filtered),sum(ntp_ind(run_order)),1);
        SStot = sum((ts_filtered - ts_mu).^2);
        SSres = sum((ts_filtered - tmp_fit_pts).^2);
        r_sq(:,cc) = 1 - (SSres ./ SStot);
    end
    
    % update count and report your progress
    prog = floor((cc/totloop) * 100);
    
    if  prog ~= ps
        disp([num2str(prog) '% complete.']);
        ps = prog;
    end
    
    clear tmp_bs tmp_fit_poly tmp_fit_pts ts_filtered SStot SSres tmp_des ts_mu
    
end

elapsedTime = toc;
disp(['Computations took ' num2str(elapsedTime/(60^2)) ' hours.']);

%% Choose the best model for each voxel
[high_rsq,best_model] = max(r_sq,[],2);


savename=[Subject_Info{1} '_Defined_PRFs_' area '_' which_exp '.mat'];
save(savename);



